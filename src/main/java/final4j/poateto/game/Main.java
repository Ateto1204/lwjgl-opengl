package final4j.poateto.game;

import final4j.poateto.engine.*;
import final4j.poateto.engine.scene.Scene;
import final4j.poateto.engine.graph.Render;

public class Main implements AppLogic {
    public static void main(String[] args) {
        Main main = new Main();
        Engine gameEngine = new Engine("Ateto Demo", new Window.WindowOptions(), main);
        gameEngine.start();
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void init(Window window, Scene scene, Render render) {

    }

    @Override
    public void input(Window window, Scene scene, long diffTimeMillis) {

    }

    @Override
    public void update(Window window, Scene scene, long diffTimeMillis) {

    }
}