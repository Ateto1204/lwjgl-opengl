package final4j.poateto.engine;

import final4j.poateto.engine.scene.Scene;
import final4j.poateto.engine.graph.Render;

public interface AppLogic {
    void cleanup();

    void init(Window window, Scene scene, Render render);
    void input(Window window, Scene scene, long diffTimeMillis);
    void update(Window window, Scene scene, long diffTimeMillis);
}
